from rest_framework import serializers
from users.models import TikUser


class TikUserSerializer(serializers.ModelSerializer):
    fname = serializers.CharField(source='user.first_name')
    lname = serializers.CharField(source='user.last_name')

    class Meta:
        model = TikUser
        fields = ('fname', 'lname', 'gender', 'grade', 'field', 'avatar', 'photo', 'city')

    def to_native(self, obj):
        """
        Serialize objects -> primitives.
        """
        ret = self._dict_class()
        ret.fields = self._dict_class()

        for field_name, field in self.fields.items():
            if field.read_only and obj is None:
                continue
            field.initialize(parent=self, field_name=field_name)
            key = self.get_field_key(field_name)
            value = field.field_to_native(obj, field_name)

            # Continue if value is None so that it does not get serialized.
            if value is None:
                continue

            method = getattr(self, 'transform_%s' % field_name, None)
            if callable(method):
                value = method(obj, value)
            if not getattr(field, 'write_only', False):
                ret[key] = value
            ret.fields[key] = self.augment_field(field, field_name, key, value)

        return ret