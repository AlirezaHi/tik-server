from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.


class TikUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    gender = models.CharField(max_length=10)
    grade = models.CharField(max_length=5)
    field = models.CharField(max_length=2)
    avatar = models.CharField(max_length=30)
    photo = models.CharField(max_length=250)
    city = models.CharField(max_length=40)


class Token(models.Model):
    phone = models.CharField(max_length=30)
    token = models.CharField(max_length=30)


class Verification(models.Model):
    phone = models.CharField(max_length=30)
    device_id = models.CharField(max_length=30)
    verification_code = models.CharField(max_length=30)


@receiver(post_save, sender=User)
def create_user_tikuser(sender, instance, created, **kwargs):
    if created:
        TikUser.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_tikuser(sender, instance, **kwargs):
    instance.tikuser.save()