from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from .serializers import *
from django.http import JsonResponse
import random, string
import urllib.request, urllib.parse
import json

# Create your views here.

#Limit of Tokens that a specific user may have
Token_Limit = 10

@api_view(['POST', 'Get', 'PUT'])
def register(request):
    if request.method == 'GET':
        phone_number = request.GET.get('id')
        if phone_number is not None:
            if User.objects.filter(username=phone_number).count() > 0:
                user = User.objects.get(username=phone_number)
                t = TikUser.objects.get(user=user)
                serializer = TikUserSerializer(t)
                return Response(serializer.data, status=201)
        return JsonResponse({'error': 'InvalidId'}, status=400)
    if request.method == 'POST':
        phone_number = request.POST.get('phone')
        device_id = request.POST.get('device_id')
        if phone_number is not None and device_id is not None:
            # Check Phone number Validation
            if phone_check(phone_number):
                #Generate 5 digit For Verification Code
                v_rand_number = random_number()
                message = urllib.parse.quote('با سلام کد تایید عضویت شما در تیک: ' +str(v_rand_number))
                print(v_rand_number)
                result = urllib.request.urlopen('https://api.kavenegar.com/v1/643754734F516158374B444C795774615835443846773D3D/sms/send.json?receptor='+phone_number+'&message='+message).read()
                content = json.loads(result.decode('utf-8'))
                # print(content)
                v = Verification.objects.filter(phone=phone_number, device_id=device_id)
                if Verification.objects.filter(phone=phone_number, device_id=device_id).count() == 1:
                    v = v.first()
                    v.verification_code = v_rand_number
                else:
                    v = Verification(phone=phone_number, device_id=device_id, verification_code=v_rand_number)
                v.save()
                #Send sms API
                return JsonResponse({'msg': 'verification code sent!'}, status=201)
            return JsonResponse({'error': 'InvalidPhoneNumber'}, status=400)
        return JsonResponse({'error': 'InvalidPhoneNumber'}, status=400)
    if request.method == 'PUT':
        #update information
        phone_number = request.data.get('user_id')
        token = request.data.get('token')
        if phone_number is not None and token is not None:
            if Token.objects.filter(phone=phone_number, token=token).count() > 0:
                u = User.objects.get(username=phone_number)
                t = TikUser.objects.get(user=u)
                if request.data.get('fname'):
                    u.first_name = request.data.get('fname')
                if request.data.get('lname'):
                    u.last_name = request.data.get('lname')
                if request.data.get('gender'):
                    t.gender = request.data.get('gender')
                if request.data.get('grade'):
                    t.grade = request.data.get('grade')
                if request.data.get('field'):
                    t.field = request.data.get('field')
                if request.data.get('avatar'):
                    t.avatar = request.data.get('avatar')
                if request.data.get('photo'):
                    t.photo = request.data.get('photo')
                if request.data.get('city'):
                    t.city = request.data.get('city')
                u.save()
                t.save()
                s = TikUserSerializer(t)
                return Response(s.data, status=201)
        return JsonResponse({
            'error': 'NotInstance'
        }, status=404)


@api_view(['POST'])
def verification(request):
    if request.method == 'POST':
        phone_number = request.POST.get('phone')
        device_id = request.POST.get('device_id')
        verification_code = request.POST.get('verification_code')
        if phone_number is not None and phone_check(phone_number) and verification_code is not None and device_id is not None:
            v = Verification.objects.filter(phone=phone_number, device_id=device_id)
            if v.count() == 1:
                if v.first().verification_code == verification_code:
                    if Token.objects.filter(phone=phone_number).count() < Token_Limit:
                        # Delete verification code to be unusable for another try of user
                        v.first().delete()
                        generated_token = token_generator()
                        token = Token(phone=phone_number, token=generated_token)
                        token.save()
                        u = User.objects.filter(username=phone_number)
                        t = TikUser.objects.filter(user=u)
                        if t.count() > 0:
                            json_response = {
                                'token': generated_token,
                                'id': phone_number
                            }
                            user = t.first()
                            if user.user.first_name:
                                json_response['fname'] = user.user.first_name
                            if user.user.last_name:
                                json_response['lname'] = user.user.last_name
                            if user.gender:
                                json_response['gender'] = user.gender
                            if user.grade:
                                json_response['grade'] = user.grade
                            if user.field:
                                json_response['field'] = user.field
                            if user.avatar:
                                json_response['avatar'] = user.avatar
                            if user.photo:
                                json_response['photo'] = user.photo
                            if user.city:
                                json_response['city'] = user.city
                            return JsonResponse(
                                {
                                    'token': generated_token,
                                    'id': phone_number,
                                    # Set City By IP here
                                    'city': city_ip(get_ip(request))
                                },
                                status=201
                            )
                        u = User(username=phone_number)
                        u.save()
                        t = TikUser(user=u)
                        t.save()
                        ip = get_ip(request)
                        city = city_ip(ip)
                        return JsonResponse(
                            {
                                'token': generated_token,
                                'id': phone_number,
                                # Set City By IP here
                                'city': city
                            },
                            status=201
                        )
                    return JsonResponse({'error': 'UnableToGetNewToken'}, status=400)
            return JsonResponse({'error': 'VerificationFailed'}, status=403)
        return JsonResponse({'error': 'VerificationFailed'}, status=403)


@api_view(['POST'])
def logout(request):
    if request.method == 'POST':
        token = request.POST.get('token')
        phone = request.POST.get('id')
        if token is not None and phone is not None:
            if Token.objects.filter(token=token, phone=phone).count() > 0:
                Token.objects.get(token=token, phone=phone).delete()
                return JsonResponse(
                    {
                        'msg': 'success!!!'
                    },
                    status=201
                )
        return JsonResponse(
            {
                'error': 'InvalidToken'
            },
            status=400
        )


def token_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def random_number():
    return str(random.randint(10000, 99999))


def phone_check(phone_number):
    phone_check_bool = True
    if len(phone_number) != 11:
        phone_check_bool = False
    for i in phone_number:
        if not i.isdigit():
            phone_check_bool = False
    return phone_check_bool


def get_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def city_ip(ip):
    url = 'http://www.freegeoip.net/json/'
    result = urllib.request.urlopen(url + 'github.com').read()
    content = json.loads(result.decode('utf-8'))
    return content['city']

