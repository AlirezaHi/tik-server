from django.conf.urls import url
from users import views

urlpatterns = [
    url(r'^$', views.register),
    url(r'^verificate', views.verification),
    url(r'^logout', views.logout),
]
