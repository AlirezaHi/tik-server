from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse, HttpResponse
from .models import *
import random, string
from users.models import Token
from django.core.mail import send_mail
from admins.serializers import *

# Create your views here.

#Limit of Tokens that a specific user may have
Token_Limit = 10

@api_view(['POST'])
def signup(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        if email is not None:
            v = VerificationAdmin(email=email)
            v.save()
            return JsonResponse(
                {
                    'msg': 'Request registered!'
                },
                status=201
            )
        return JsonResponse({
            'error': 'InvalidEmail'
        }, status=400)


@api_view(['POST'])
def verification(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        if email is not None:
            if VerificationAdmin.objects.filter(email=email).count() > 0:
                VerificationAdmin.objects.get(email=email).delete()
                generated_password = password_generator()
                if AdminUser.objects.filter(user__email=email).count() <= 0:
                    u = User(username=email, email=email, password=generated_password)
                    u.save()
                    a = AdminUser(user=u)
                    a.save()
                    # sending email - here have to put sending mail address
                    print(generated_password)
                    # send_mail('Tik Registeration', 'password : ' + generated_password, 'sender email here', [email])
                return JsonResponse(
                    {
                        'msg': 'user verificated!'
                    },
                    status=200
                )
        return JsonResponse({
            'error': 'NoSuchEmailRegistered'
        }, status=404)


@api_view(['PUT', 'GET'])
def profile_admin(request):
    if request.method == 'PUT':
        email = request.data.get('id')
        if email is not None:
            if User.objects.filter(email=email).count() > 0:
                u = User.objects.get(email=email)
                a = AdminUser.objects.get(user=u)
                if request.data.get('fname'):
                    a.user.first_name = request.data.get('fname')
                if request.data.get('lname'):
                    a.user.last_name = request.data.get('lname')
                if request.data.get('gender'):
                    a.gender = request.data.get('gender')
                if request.data.get('field'):
                    a.field = request.data.get('field')
                if request.data.get('university'):
                    a.university = request.data.get('university')
                if request.data.get('enterance_year'):
                    a.enterance_year = request.data.get('enterance_year')
                if request.data.get('avatar'):
                    a.avatar = request.data.get('avatar')
                if request.data.get('bio'):
                    a.bio = request.data.get('bio')
                a.user.save()
                a.save()
                a = AdminUserSerializer(a)
                return Response(a.data, status=201)
        return JsonResponse({
            'error': 'NotInstance'
        }, status=404)
    if request.method == 'GET':
        token = request.GET.get('token')
        email = request.GET.get('id')
        if token is not None and email is not None:
            if Token.objects.filter(token=token).count() > 0 or AdminToken.objects.filter(token=token).count() > 0:
                u = User.objects.get(email=email)
                a = AdminUser.objects.get(user=u)
                a = AdminUserSerializer(a)
                return Response(a.data, status=201)
        return JsonResponse({'error': 'InvalidId'}, status=400)


@api_view(['POST'])
def change_password(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        old_password = request.POST.get('old_password')
        new_password = request.POST.get('new_password')
        if email is not None and old_password is not None and new_password is not None:
            if User.objects.filter(email=email).count() > 0:
                u = User.objects.get(email=email)
                if u.password == old_password:
                    if password_validation(new_password):
                        u.password = new_password
                        u.save()
                        return JsonResponse({
                            'msg': 'changed !!!'
                        }, status=200)
                    return JsonResponse({
                        'error': 'BadPassword'
                    }, status=400)
        return JsonResponse({
            'error': 'IncorrectLogin'
        }, status=401)


@api_view(['POST'])
def login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        forgot = None
        if request.POST.get('forgot') is not None:
            forgot = bool(request.POST.get('forgot'))
        if email is not None and password is not None:
            if User.objects.filter(email=email).count() > 0:
                u = User.objects.get(email=email)
                if forgot  and forgot == True:
                    new_password = password_generator()
                    u.password = new_password
                    u.save()
                    #sending email - here have to put sending mail address
                    print(new_password)
                    send_mail('Tik Registeration', 'password : '+new_password, 'sender email here', [email])
                    return JsonResponse({
                        'msg': 'reset password mail sent !!!'
                    }, status=201)
                elif u.password == password:
                    if AdminToken.objects.filter(email=email).count() < Token_Limit:
                        generated_token = token_generator()
                        t = AdminToken(email=email, token=generated_token)
                        t.save()
                        return JsonResponse({
                            'token': generated_token
                        }, status=200)
                    return JsonResponse({
                        'error': 'UnableToGetNewToken'
                    }, status=400)
        return JsonResponse({
            'error': 'IncorrectLogin'
        }, status=401)


@api_view(['POST'])
def assign_group(request):
    if request.method == 'POST':
        email = request.POST.get('id')
        group_id = request.POST.get('group')
        if AdminUser.objects.filter(user__email=email).count() > 0:
            a = AdminUser.objects.get(user__email=email)
            if Groups.objects.filter(group_id=group_id).count() > 0:
                a.groups.add(Groups.objects.get(group_id=group_id))
            else:
                a.groups.create(group_id=group_id)
            a.save()
            return JsonResponse({
                'msg': 'assigned!'
            }, status=200)
        return JsonResponse({
            'error': 'NoSuchAdmin'
        }, status=404)


@api_view(['POST'])
def has_permission(request):
    if request.method == 'POST':
        email = request.POST.get('id')
        project_id = request.POST.get('project')
        if AdminUser.objects.filter(user__email=email).count() > 0:
            if AdminUser.objects.filter(groups__permissions__project_id=project_id).count() > 0:
                return HttpResponse(status=200)
            return JsonResponse({
                'error:': 'ProjectNotFound'
            }, status=404)
        return HttpResponse(status=401)


@api_view(['POST'])
def permit_group(request):
    if request.method == 'POST':
        project_id = request.POST.get('project')
        group_id = request.POST.get('groupId')
        if project_id is not None and group_id is not None:
            if Groups.objects.filter(group_id=group_id,permissions__project_id=project_id).count() > 0:
                return HttpResponse(status=201)
            elif Project.objects.filter(project_id=project_id).count() > 0:
                Groups.objects.get(group_id=group_id)
            #here have to be completed


@api_view(['POST'])
def permit_user(request):
    # it has to be completerd
    return None




def password_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def password_validation(password):
    return len(password) > 7


def token_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))