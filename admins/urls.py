from django.conf.urls import url
from admins import views

urlpatterns = [
    url(r'^admin/verificate', views.verification),
    url(r'^admin', views.assign_group),
    url(r'^profile_admin', views.profile_admin),
    url(r'^signup', views.signup),
    url(r'^login', views.login),
    url(r'^login/change_password', views.change_password),
    url(r'^auth/', views.has_permission),
    url(r'^auth/group', views.permit_group),
    url(r'^auth/user', views.permit_user),
]
