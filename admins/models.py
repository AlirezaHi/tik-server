from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    project_id = models.CharField(max_length=50)


class Groups(models.Model):
    group_id = models.CharField(max_length=50)
    permissions = models.ManyToManyField(Project)


class AdminUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    gender = models.CharField(max_length=10)
    field = models.CharField(max_length=2)
    university = models.CharField(max_length=50)
    enterance_year = models.IntegerField(default=1396)
    avatar = models.CharField(max_length=30)
    bio = models.TextField(max_length=250)
    groups = models.ManyToManyField(Groups)


class VerificationAdmin(models.Model):
    email = models.CharField(max_length=60)


class AdminToken(models.Model):
    email = models.CharField(max_length=60)
    token = models.CharField(max_length=30)