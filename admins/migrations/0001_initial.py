# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-07-28 15:32
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdminToken',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=60)),
                ('token', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='AdminUser',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('gender', models.CharField(max_length=10)),
                ('field', models.CharField(max_length=2)),
                ('university', models.CharField(max_length=50)),
                ('enterance_year', models.IntegerField(default=1396)),
                ('avatar', models.CharField(max_length=30)),
                ('bio', models.TextField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Groups',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_id', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('project_id', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='VerificationAdmin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=60)),
            ],
        ),
        migrations.AddField(
            model_name='groups',
            name='permissions',
            field=models.ManyToManyField(to='admins.Project'),
        ),
        migrations.AddField(
            model_name='adminuser',
            name='groups',
            field=models.ManyToManyField(to='admins.Groups'),
        ),
    ]
